package com.jixcayau.alertify

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class AlertifyApplication

fun main(args: Array<String>) {
    runApplication<AlertifyApplication>(*args)
}
